#!/usr/bin/env bash

cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

# Install k8s command line apps
sudo yum install -y kubelet kubeadm kubectl
systemctl enable kubelet
systemctl start kubelet

# Add necessary K8s Ports
sudo firewall-cmd --permanent --add-port=10251/tcp # Slave & Master
sudo firewall-cmd --permanent --add-port=10255/tcp # Slave & Master
sudo firewall-cmd --reload

# Set the net.bridge.bridge-nf-call-iptables to ‘1’ in your sysctl config file. 
# This ensures that packets are properly processed by IP tables during filtering and port forwarding.
cat <<EOF > /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system

# Pull necessary Kubeadm images
kubeadm config images pull
