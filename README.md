# Geoffrey4Centos7

To setup Geoffrey on a Centos 7 Host use this repo can be used as an example

## Installation

To get setup execute the setup.sh file on the Centos7 VM.

```
curl -s 'https://bitbucket.org/fxqlabs-oss/geoffrey-centos7/raw/master/setup.sh' | bash
```

This will install Geoffrey and configure it to use the choices.yml located within this repo.

## Usage

Once the installation is complete simply type `geoffrey` and hit enter to be presented with the CLI options
as defined in the repos `choices.yml`

## Contributing
This repo is for internal use but feel free to use it as a base for your own configuration

## License
[MIT](https://choosealicense.com/licenses/mit/)