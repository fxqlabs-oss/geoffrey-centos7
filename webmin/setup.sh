#!/usr/bin/env bash

yum install -y wget
cd /etc/yum.repos.d/ && wget https://bitbucket.org/fxqlabs-oss/geoffrey-centos7/raw/master/webmin/webmin.repo
cd /tmp && wget https://download.webmin.com/jcameron-key.asc && rpm --import jcameron-key.asc
yum install -y webmin
firewall-cmd --permanent --zone=public --add-port=10000/tcp && firewall-cmd --reload
systemctl start webmin
systemctl enable webmin