#!/usr/bin/env bash

#Install Python 3.6
sudo yum install -y epel-release gcc
sudo yum install -y python36 python36-pip python36-devel

# Install geoffrey
pip3 install fxq-geoffrey

# Install custom config
yum -y install wget
mkdir -p /root/.config/geoffrey
cd /root/.config/geoffrey
wget https://bitbucket.org/fxqlabs-oss/geoffrey-centos7/raw/master/config.ini